# /usr/bin/python

from bot import main
from daemonize import Daemonize

pid = "/tmp/reackonfood_bot.pid"

daemon = Daemonize(app="reckonfood_bot", pid=pid, action=main)
daemon.start()
