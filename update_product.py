

import csv
from pymongo import MongoClient


headerInd = 1
items_starts = 2
products_list = []

from myconfig import remoteURL, localURL

client = MongoClient(localURL)
db = client.reckonfood

db.products.remove({})

with open('./csv/products.csv', newline='', encoding='utf-8') as f:
    reader = csv.reader(f)
    for ind, row in enumerate(reader):
        if ind == headerInd:
            print(row)
            header = row
        if ind >= items_starts:
            product = {}
            for i in range(len(header)):
                product['id'] = ind
                product[header[i]] = row[i]
                
            products_list.append(product)

db.products.insert_many(products_list)
