# -*- coding: utf-8 -*-


from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
from pymongo import MongoClient
from bson.objectid import ObjectId
from main import summarize_dishes


import re
from myconfig import remoteURL, telegramKEY, localURL


updater = Updater(telegramKEY)

dish_symbol = '$'
cat_symbol = '@'

client = MongoClient(localURL)
db = client.reckonfood
dish_list = []
menu_items = db.dishes.distinct('type')


def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="/menu - Для просмотра меню")

def sendMenu(bot, update):
    custom_keyboard = [["Салаты", "Основные блюда"], ["Напитки"], ["/cleancart", "/showcart", "/checkout"]]
    reply_markup = ReplyKeyboardMarkup(custom_keyboard)
    bot.send_message(chat_id=update.message.chat_id, text="Братишка, смотри что в меню", reply_markup=reply_markup)


def additem(bot, update):
    item = update.message.text[5:]

    if db.cart.find_one({'chat_id' : update.message.chat_id }) != None:
        db.cart.update({'chat_id' : update.message.chat_id}, {"$addToSet": { 'items': item}})
    else:
        db.cart.insert({'chat_id' : update.message.chat_id, 'items': item})

    text_add = "Добавлен в корзину"
    bot.send_message(chat_id=update.message.chat_id, text=text_add)

def removeitem(bot, update):
    if db.cart.find_one({'chat_id' : update.message.chat_id }) != None:
        db.cart.update({'chat_id' : update.message.chat_id}, {"$pull": { 'items': args[0]}})


def showcart(bot, update):
    cart = db.cart.find_one({'chat_id' : update.message.chat_id })
    try:
        if len(cart[u'items']) == 0:
            bot.send_message(parse_mode='HTML', chat_id=update.message.chat_id, text="<b>Ваша корзина пуста</b>")
        html = []
        for i in cart[u'items']:
            html.append(db.dishes.find_one({"_id": ObjectId(i)})['name'])
        bot.send_message(parse_mode='HTML', chat_id=update.message.chat_id, text=('\n').join(html))
    except KeyError:
        bot.send_message(parse_mode='HTML', chat_id=update.message.chat_id, text="<b>Ваша корзина пуста</b>")


def cleancart(bot, update):

    db.cart.remove({'chat_id' : update.message.chat_id })
    bot.send_message(parse_mode='HTML', chat_id=update.message.chat_id, text="<b>Ваша корзина пуста</b>")

def checkout(bot, update):

    print update.message.chat_id
    cart_items = db.cart.find_one({'chat_id': update.message.chat_id})['items']
    sum_dict = summarize_dishes(cart_items)[0]

    check_sum = []
    for key in sum_dict.keys():
        line = key + ': ' + str(sum_dict[key]) 
        check_sum.append(line)

    fat_level = int(float(sum_dict[u'Калорийность'])/2500)
    if fat_level > 3:
        bot.send_video(chat_id=update.message.chat_id, video=open('./rocks/tomuch.mp4', 'rb'))
    else: rock = 'rocks/' + str(fat_level) + '!.png'
    bot.send_photo(chat_id=update.message.chat_id, photo=open(rock, 'rb'))
    bot.send_message(parse_mode='HTML', chat_id=update.message.chat_id, text=('\n').join(check_sum))

def video(bot, update):
    bot.send_video(chat_id=update.message.chat_id, video=open('./rocks/tomuch.mp4', 'rb'))

def explain(bot, update):
    cart_items = db.cart.find_one({'chat_id': update.message.chat_id})['items']
    items_in_cart = summarize_dishes(cart_items)[1]
    for item in items_in_cart:
        item_sum = []
        for key in item.keys():
            line = key + ': ' + str(item[key]) 
            item_sum.append(line)

        bot.send_message(parse_mode='HTML', chat_id=update.message.chat_id, text=('\n').join(item_sum))
def echo(bot, update):
    print type(update.message.text)
    print len(update.message.text)

    # if update.message.text == u"Напитки":
    #     print('half-way')
    #     cursor = db.drinks.find({})
    #     dr_list = []
        #     for drk in cursor:
    #         dr_list.append(InlineKeyboardButton(drk['name'], callback_data=str(drk['_id'])))
            #     drk_markup = InlineKeyboardMarkup(build_menu(dr_list, n_cols=1))
            #     bot.send_message(chat_id=update.message.chat_id, text="Чрезмерное употребление вредит Вашему здоровью", reply_markup=drk_markup)


    if update.message.text in menu_items:
        cursor = db.dishes.find({'type': update.message.text})
        button_list = []

        for dish in cursor:
            button_list.append(InlineKeyboardButton(dish['name'], callback_data=str(dish['_id'])))

        reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=1))
        bot.send_message(chat_id=update.message.chat_id, text="Бери ещё, не отказывай себе и не лопни", reply_markup=reply_markup)


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu



def button(bot, update):
    query = update.callback_query
    item_id = query.data
    chat_id = query.message.chat_id
    if item_id == '5a3f97e795c26869ed6b3ecc':
        bot.send_video(text="Поздравляю, вы побели это лучший выбор",chat_id=chat_id, video=open('./rocks/beer.mp4', 'rb'))
        bot.send_message(chat_id=chat_id, text="Поздравляю, Вы, победили. Это лучший выбор за сотку!")
    if db.cart.find_one({'chat_id' : chat_id }) != None:
        db.cart.update({'chat_id' : chat_id}, {"$push": { 'items': item_id}})
    else:
        db.cart.insert({'chat_id' : chat_id, 'items': [item_id]})
    bot.edit_message_text(text="Молодец, бери еще", chat_id=query.message.chat_id, message_id=query.message.message_id)


def main():
    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CommandHandler('menu', sendMenu))
    updater.dispatcher.add_handler(CommandHandler('checkout', checkout))
    updater.dispatcher.add_handler(CommandHandler('explain', explain))
    updater.dispatcher.add_handler(CommandHandler('showcart', showcart))
    updater.dispatcher.add_handler(CommandHandler('cleancart', cleancart))
    updater.dispatcher.add_handler(CommandHandler('video', video))
    updater.dispatcher.add_handler(CallbackQueryHandler(button))

    echo_handler = MessageHandler(Filters.text, echo)
    updater.dispatcher.add_handler(echo_handler)

    updater.start_polling()
    updater.idle()

if __name__ == "__main__":
	print "Test"
	main()
