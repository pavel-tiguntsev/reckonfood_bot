# -*- coding: utf-8 -*-
import csv
from pymongo import MongoClient

from myconfig import remoteURL, localURL

dish_symbol = '$'
cat_symbol = '@'

client = MongoClient(localURL)
db = client.reckonfood
dish_list = []
db.dishes.remove({})

with open('./csv/recs/all.csv', newline='', encoding='utf-8') as f:
    reader = csv.reader(f)
    dish = {}
    for ind, row in enumerate(reader):
        if row[0][0] == cat_symbol:
            print(row[0][1:])
            dish['type'] = row[0][1:]
        elif row[0][0] == dish_symbol:
            if 'name' in dish:
                dish_list.append(dish)
            dish = {'type': dish['type']}
            print(row[0])
            dish['name'] = row[0][1:]
            dish['ings'] = []
        else:
            dish['ings'].append({'product': row[0], 'q': row[3]})
    dish_list.append(dish)

for i in range(len(dish_list)):
    dish_list[i]['id'] = i

db.dishes.insert_many(dish_list)
